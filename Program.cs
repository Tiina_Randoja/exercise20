﻿using System;

namespace exercise20
{
    class Program
    {
        static void Main(string[] args)
        { 
        //Start the program with Clear();
        Console.Clear();
        
        //TASK A

        //Ask for 2 numbers, save as variables
        Console.WriteLine("Hi. Would you please type in a number?");
        var number1 = int.Parse(Console.ReadLine());
        Console.WriteLine("Cool. Could you type in another number?");
        var number2 = int.Parse(Console.ReadLine());
        Console.WriteLine($"Thanks.. ");
        
        //Option 1 Number1>Number2
        if (number1 > number2)
        {Console.WriteLine($"I see .. {number2} is smaller than {number1} .. but you knew that.. ");
        
        //Option 2 Number1<Number2
        }
         if (number1 < number2)
        {Console.WriteLine($"I see .. {number1} is smaller than {number2} .. but you knew that.. ");
        
        }
        //Option 3 Number1=Number2
        {
            Console.WriteLine($"I see.. {number1} is equal to {number2}");
        }
        //TASK B

        //Ask for a password (at elast 8 characters)
        Console.WriteLine("Now.. Type in a password that is at least 8 characters long");
        
        //Set variables for the password and lenght count
        var password = Console.ReadLine();
        var lenght = password.Length;
        
        //Option 1&2, pass (8 characters or more)
        if (lenght == 8)

        {
        Console.WriteLine("Your new password has been set");
        Console.WriteLine("Press enter to continue");
        Console.ReadKey();
        }
         if (lenght > 8)
        {
            Console.WriteLine("Your new password has been set");
            Console.WriteLine("Press enter to continue");
            Console.ReadKey();
            }

        //Option 3, no pass (not enough characters)    
        if (lenght < 8)
        {
        Console.WriteLine("Nope.. Need more characters.");
        Console.WriteLine("Press enter to continue");
        Console.ReadKey();
        }
       
        //TASK C 

        //Changing the password

        Console.WriteLine("But I just like this password. Type in another one, I'll change it");
        Console.WriteLine("Type in the new password");
        var newpassword = Console.ReadLine();
        password = password.Replace(password, newpassword);

        Console.WriteLine($"Your new password is {password} ");
        
    
        
        
        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine();
        Console.WriteLine("Press <Enter> to quit the program");
        Console.ReadKey();
        }
    }
}
